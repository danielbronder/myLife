
import UIKit

class ViewController: UIViewController {
    
    var person: Person?
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    @IBOutlet weak var imageButton: UIButton!
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var ageSlider: UISlider!
    
    @IBOutlet weak var ageValue: UILabel!
    
    @IBAction func cancelButton(_ sender: UIBarButtonItem) {
        let isPresentingInAddMode = presentedViewController is UINavigationController
        
        if isPresentingInAddMode {
            dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        Editing existing person
        if let person = person {
            nameTextField.text = person.name
            imageButton.setImage(person.image, for: [])
            
            if let age = person.age {
                ageSlider.value = age
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let sender = sender as? UIBarButtonItem!, sender === doneButton {
            let name = nameTextField.text
            let image = imageButton.image(for: [])
            let age = ageSlider.value
//            ageValue.setValue(NSString(format: "%.1f", ageSlider.value), forKey: String)
         
//            Set new person
            person = Person(name: name, image: image, age: age)
        }
    }
    
    func testFieldShouldReturn(_ textfield: UITextField) -> Bool {
        textfield.resignFirstResponder()
        return true
    }
}
