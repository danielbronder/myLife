import UIKit

class Person {
    
   public var name: String?
   public var image: UIImage?
   public var age: Float?
    
    init(name: String? = nil, image: UIImage? = nil, age: Float? = nil) {
        self.name = name;
        self.image = image;
        self.age = age;
    }
}
