import UIKit

class PeopleTableController: UITableViewController {
    
    var people = [Person]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadSampleData()
        title = "My Life"
    }
    
    func loadSampleData() {
        let person1 = Person(
            name: "Daniel",
            image: UIImage(named: "Sample1"),
            age: 20
        )
        
        people = [person1]
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let person = people[indexPath.row]
        
        cell.textLabel!.text = person.name
        cell.imageView!.image = person.image
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddPersonSegue" {
            print("Adding new Person")
        } else if segue.identifier == "ShowPersonSegue" {
            let destinationController = segue.destination as! ViewController
            let selectedTableCell = sender as! UITableViewCell
            let indexPath = tableView.indexPath(for: selectedTableCell)
            let person = people[indexPath!.row]
            destinationController.person = person
        }
    }
    
    @IBAction func unwinToPersonList (_ sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? ViewController, let person = sourceViewController.person {
            
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                people[selectedIndexPath.row] = person
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            } else {
                let newIndexPath = IndexPath(row: people.count, section: 0)
                people.append(person)
                tableView.insertRows(at: [newIndexPath], with: .bottom)

            }
        }
    }
}
